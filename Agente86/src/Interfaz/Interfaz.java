package Interfaz;
import java.awt.EventQueue;
import javax.swing.JFrame;

public class Interfaz 
{
	private JFrame ventana;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz window = new Interfaz();
					window.ventana.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interfaz() 
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{
		crearVentana();
		PantallaPrincipal pantallaPrincipal = new PantallaPrincipal(ventana);
		pantallaPrincipal.iniciar();
	}

	private void crearVentana()
	{
		ventana = new JFrame("Agente86");
		ventana.setBounds(0, 0, Configuracion.VENTANA_ANCHO, Configuracion.VENTANA_ALTO);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.setFocusable(true);
		ventana.setLocationRelativeTo(null);
		ventana.setIconImage(Herramientas.cargarImagen("agente.png"));
	}
}

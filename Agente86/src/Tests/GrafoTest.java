package Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

import Negocio.Arista;
import Negocio.Grafo;

public class GrafoTest 
{
	@Test
	public void cantidadVertices()
	{
		Coordinate c1 = new Coordinate(34,454);
		Coordinate c2 = new Coordinate(34,2);
		Coordinate c3 = new Coordinate(3,2);
		Grafo g = new Grafo();
		g.agregarArista(c1, c2);
		g.agregarArista(c1, c3);
		g.agregarArista(c3, c2);
		assertTrue(g.cantVertices() == 3);
	}
	
	@Test
	public void sinVertices()
	{
		Grafo g = new Grafo();
		assertTrue(g.cantVertices() == 0);
	}
	
	@Test
	public void agregarArista()	
	{
		Coordinate c1 = new Coordinate(34,454);
		Coordinate c2 = new Coordinate(34,2);
		Coordinate c3 = new Coordinate(3,2);
		Grafo g = new Grafo();
		g.agregarArista(c1, c2);
		g.agregarArista(c1, c3);
		Arista a = new Arista(c1,c2);
		Arista b = new Arista(c1,c3);
		assertTrue (g.getAristas().get(0).equals(a) &&g.getAristas().get(1).equals(b));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void agregarBucle()
	{
		Coordinate c1 = new Coordinate(34,454);
		Grafo g = new Grafo();
		g.agregarArista(c1, c1);	
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void agregarAristaExistente()
	{
		Coordinate c1 = new Coordinate(34,454);
		Coordinate c2 = new Coordinate(34,2);
		Grafo g = new Grafo();
		g.agregarArista(c1, c2);
		g.agregarArista(c2, c1);
	}
	
	@Test
	public void eliminarArista()	
	{
		Coordinate c1 = new Coordinate(34,454);
		Coordinate c2 = new Coordinate(34,2);
		Coordinate c3 = new Coordinate(3,2);
		Grafo g = new Grafo();
		g.agregarArista(c1, c2);
		g.agregarArista(c1, c3);
		g.eliminarArista(c1, c2);
		Arista a = new Arista(c1,c3);
		assertTrue (g.getAristas().get(0).equals(a) && g.cantAristas()==1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void eliminarAristaInexistente()	
	{
		Coordinate c1 = new Coordinate(34,454);
		Coordinate c2 = new Coordinate(34,2);
		Grafo g = new Grafo();
		g.agregarArista(c1, c2);
		g.eliminarArista(c1, c2);
		g.eliminarArista(c1, c2);
	}
	
	@Test
	public void verificarVertices()
	{
		Coordinate c1 = new Coordinate(34,454);
		Coordinate c2 = new Coordinate(2,54);
		Coordinate c3 = new Coordinate(10,454);
		
		Grafo g = new Grafo();
		g.agregarArista(c1, c2);
		g.agregarArista(c1, c3);
		g.agregarArista(c2, c3);
		
		ArrayList <Coordinate> vertices = new ArrayList<Coordinate>();
		vertices.add(c1);
		vertices.add(c2);
		vertices.add(c3);
		
		assertTrue (vertices.equals(g.getVertices()));
	}
	
	@Test 
	public void getAristasIncidentes()
	{
	
		Coordinate c1 = new Coordinate(34,454);
		Coordinate c2 = new Coordinate(34,2);
		Coordinate c3 = new Coordinate(3,2);
		Coordinate c4 = new Coordinate(123,-2);
		
		Grafo g = new Grafo();
		g.agregarArista(c1, c2);
		g.agregarArista(c1, c4);
		g.agregarArista(c3, c1);
		g.agregarArista(c2, c3);
		
		ArrayList<Arista> aristasIncidentes = new ArrayList<Arista>();
		Arista a= new Arista(c1,c2);
		Arista b= new Arista(c4,c1);
		Arista c= new Arista(c3,c1);
		
		aristasIncidentes.add(a);
		aristasIncidentes.add(b);
		aristasIncidentes.add(c);
		assertTrue(aristasIncidentes.equals(g.getAristasIncidentes(c1)));
		
	}
	
	@Test (expected= IllegalArgumentException.class)
	public void sinAristasIncidentes()
	{
		Coordinate c1 = new Coordinate(34,454);
		Coordinate c2 = new Coordinate(34,2);
		Coordinate c3 = new Coordinate(3,2);
		Coordinate c4 = new Coordinate(123,-2);
		
		Grafo g = new Grafo();
		g.agregarArista(c2, c4);
		g.agregarArista(c2, c3);
		g.getAristasIncidentes(c1);
	
	}
	
	@Test
	public void grafoCompleto() 
	{
		ArrayList<Coordinate> vertices = new ArrayList<Coordinate>();
		Coordinate c1 = new Coordinate(34,454);
		Coordinate c2 = new Coordinate(2,54);
		Coordinate c3 = new Coordinate(10,454);
		Coordinate c4 = new Coordinate(10,10);
		vertices.add(c1);
		vertices.add(c2);
		vertices.add(c3);
		vertices.add(c4);
		Grafo grafo = new Grafo();
		grafo.convertirGrafoCompleto(vertices);
		
		assertTrue(grafo.existeArista(new Arista(c1,c2))
				&& grafo.existeArista(new Arista(c1,c3))
				&& grafo.existeArista(new Arista(c1,c4))
				&& grafo.existeArista(new Arista(c2,c3))
				&& grafo.existeArista(new Arista(c2,c4))
				&& grafo.existeArista(new Arista(c3,c4))
				&& grafo.cantAristas()==6);
	}
	
	@Test
	public void equals()
	{
		Coordinate c1 = new Coordinate(34,454);
		Coordinate c2 = new Coordinate(2,54);
		Coordinate c3 = new Coordinate(10,454);
		Coordinate c4 = new Coordinate(10,10);
		
		Grafo g = new Grafo();
		Grafo comparar = new Grafo();
		
		g.agregarArista(c1, c2);
		g.agregarArista(c2, c4);
		g.agregarArista(c1, c3);
		
		
		comparar.agregarArista(c2, c1);
		comparar.agregarArista(c4, c2);
		comparar.agregarArista(c3, c1);
		
		assertTrue(g.equals(comparar));
	}
	
	@Test
	public void equalsConDiferenteObjeto()
	{
		Coordinate c1 = new Coordinate(34,454);
		Coordinate c2 = new Coordinate(2,54);
		Coordinate c3 = new Coordinate(10,454);
		Coordinate c4 = new Coordinate(10,10);
		
		Grafo g = new Grafo();
		g.agregarArista(c1, c2);
		g.agregarArista(c2, c4);
		g.agregarArista(c1, c3);	
		assertFalse(g.equals(c1));
	}
	
	@Test
	public void equalsConNull()
	{
		Coordinate c1 = new Coordinate(34,454);
		Coordinate c2 = new Coordinate(2,54);
		Coordinate c3 = new Coordinate(10,454);
		Coordinate c4 = new Coordinate(10,10);
		
		Grafo g = new Grafo();
		g.agregarArista(c1, c2);
		g.agregarArista(c2, c4);
		g.agregarArista(c1, c3);
	
		assertFalse(g.equals(null));
	}
	
	@Test
	public void notEquals()
	{
		Coordinate c1 = new Coordinate(34,454);
		Coordinate c2 = new Coordinate(2,54);
		Coordinate c3 = new Coordinate(10,454);
		Coordinate c4 = new Coordinate(10,10);
		
		Grafo g = new Grafo();
		g.agregarArista(c1, c2);
		g.agregarArista(c2, c4);
		g.agregarArista(c1, c3);
	
		Grafo comparar = new Grafo();
		comparar.agregarArista(c2, c4);
		
		assertFalse(g.equals(comparar));
	}
	
	@Test
	public void getAristaMaxima()
	{
		Coordinate c1 = new Coordinate(34,454);
		Coordinate c2 = new Coordinate(1,2);
		Coordinate c4 = new Coordinate(123,2);
		
		Grafo g = new Grafo();
		g.agregarArista(c1, c2);
		g.agregarArista(c1, c4);
		Arista maxima = new Arista(c1,c2);
		
		assertTrue(maxima.equals(g.getAristaMaxima()));
	}
	
	@Test
	public void getAristaMinima()
	{
		Coordinate c1 = new Coordinate(34,454);
		Coordinate c2 = new Coordinate(1,2);
		Coordinate c4 = new Coordinate(3,2);
		
		Grafo g = new Grafo();
		g.agregarArista(c1, c2);
		g.agregarArista(c1, c4);
		g.agregarArista(c2, c4);
		Arista maxima = new Arista(c2,c4);
		
		assertTrue(maxima.equals(g.getAristaMinima()));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void sinMaximo()
	{
		Grafo g = new Grafo();
		g.getAristaMaxima();
	}

	@Test (expected = IllegalArgumentException.class)
	public void sinMinimo()
	{
		Grafo g = new Grafo();
		g.getAristaMinima();
	}
}
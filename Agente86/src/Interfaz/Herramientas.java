package Interfaz;

import java.awt.Image;

import javax.swing.ImageIcon;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class Herramientas 
{
	public static double distanciaEntreCoordenadas(Coordinate c1, Coordinate c2)
	{
		double lat1 = c1.getLat();
		double lat2 = c2.getLat();
		double lng1 = c1.getLon();
		double lng2 = c2.getLon();
		
		//double radioTierra = 3958.75;//en millas  
        double radioTierra = 6371;//en kilómetros  
        double dLat = Math.toRadians(lat2 - lat1);  
        double dLng = Math.toRadians(lng2 - lng1);  
        double sindLat = Math.sin(dLat / 2);  
        double sindLng = Math.sin(dLng / 2);  
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)  
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));  
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));  
        double distancia = radioTierra * va2;  
   
        return redondear(distancia);
	}
	
	public static double redondear(double numero)
	{
		return (double)(Math.round(numero * 10d) / 10d);
	}
	
	public static Image cargarImagen(String nombreImagen)
	{
		return cargarImagenConEscala(nombreImagen,0,0);
	}
	
	public static Image cargarImagenConEscala(String nombreImagen, int ancho, int alto) 
	{	
		Image imagen;
		imagen = new ImageIcon(ClassLoader.getSystemResource(nombreImagen)).getImage();
		int width = (int) ancho;
		int height = (int) alto;
		if (ancho == 0 && alto == 0)
			return imagen;
		else
			return imagen.getScaledInstance(width, height, Image.SCALE_SMOOTH);
	}
}

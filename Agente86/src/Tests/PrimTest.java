package Tests;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import Negocio.Arista;
import Negocio.Grafo;
import Negocio.Prim;

public class PrimTest 
{
	@Test
	public void testPrimPar() 
	{	
		Coordinate v1 = new Coordinate(1,0);
		Coordinate v2 = new Coordinate(1,1);
		Coordinate v3 = new Coordinate(2,3);
		Coordinate v4 = new Coordinate(4,5);
		
		Grafo g = new Grafo();
		g.agregarArista(v1, v2);
		g.agregarArista(v1, v3);
		g.agregarArista(v1, v4);
		g.agregarArista(v2, v3);
		g.agregarArista(v2, v4);
		g.agregarArista(v3, v4);
		
		Grafo agm = Prim.getAGM(g);
		Grafo arbol = new Grafo();
		arbol.agregarArista(v1, v2);
		arbol.agregarArista(v2, v3);
		arbol.agregarArista(v3, v4);
		
		assertTrue(arbol.equals(agm));
	}
	
	@Test
	public void testPrimImpar() 
	{	
		Coordinate v1 = new Coordinate(1,0);
		Coordinate v2 = new Coordinate(1,1);
		Coordinate v3 = new Coordinate(2,3);
		Coordinate v4 = new Coordinate(5,5);
		Coordinate v5 = new Coordinate(4,5);
		
		Grafo g = new Grafo();
		g.agregarArista(v1, v2); 
		g.agregarArista(v1, v3);
		g.agregarArista(v1, v4);
		g.agregarArista(v1, v5);
		g.agregarArista(v2, v3);
		g.agregarArista(v2, v4);
		g.agregarArista(v2, v5);
		g.agregarArista(v3, v4);
		g.agregarArista(v3, v5);
		g.agregarArista(v4, v5);
		
		Grafo agm = Prim.getAGM(g);
		Grafo arbol = new Grafo();
		arbol.agregarArista(v2, v1);
		arbol.agregarArista(v2, v3);
		arbol.agregarArista(v3, v5);
		arbol.agregarArista(v4, v5);
		
		assertTrue(arbol.equals(agm));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void PrimInvalido()
	{
		Grafo g = new Grafo();
		Grafo h =Prim.getAGM(g);
	}
	
	@Test
	public void getAristaMinima ()
	{
		Coordinate v1 = new Coordinate(221,1222);
		Coordinate v2 = new Coordinate(2,3);
		Coordinate v3 = new Coordinate(2,1);
		Coordinate v4 = new Coordinate(565,500);
		
		Grafo g = new Grafo();
		g.agregarArista(v1, v2);
		g.agregarArista(v1, v3);
		g.agregarArista(v1, v4);
		g.agregarArista(v2, v4);
		g.agregarArista(v3, v4);
		g.agregarArista(v3, v2);
		Arista minima = new Arista(v3,v2);
		ArrayList<Coordinate> verticesRecorridos = new ArrayList<Coordinate>();
		verticesRecorridos.add(v1);
		verticesRecorridos.add(v4);
		verticesRecorridos.add(v3);
		assertTrue( minima.equals(Prim.getAristaMinima(v3, verticesRecorridos, g)));
	}
	
	@Test
	public void  NoHayAristaMinima()
	{
		Coordinate v1 = new Coordinate(221,1222);
		Coordinate v2 = new Coordinate(2,3);
		Coordinate v3 = new Coordinate(2,1);
		Coordinate v4 = new Coordinate(565,500);
		
		Grafo g = new Grafo();
		g.agregarArista(v1, v2);
		g.agregarArista(v1, v3);
		g.agregarArista(v1, v4);
		g.agregarArista(v2, v4);
		g.agregarArista(v3, v4);
		g.agregarArista(v3, v2);
		Arista minima = null;
		ArrayList<Coordinate> verticesRecorridos = new ArrayList<Coordinate>();
		verticesRecorridos.add(v1);
		verticesRecorridos.add(v4);
		verticesRecorridos.add(v3);
		verticesRecorridos.add(v2);
		assertTrue( minima == (Prim.getAristaMinima(v3, verticesRecorridos, g)));
	}
}
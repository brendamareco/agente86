package Negocio;
import org.openstreetmap.gui.jmapviewer.Coordinate;

import Interfaz.Herramientas;

public class Arista 
{
	private Coordinate origen;
	private Coordinate destino;
	private double peso;
	
	public Arista(Coordinate origen, Coordinate destino)
	{
		if (origen.equals(destino))
			throw new IllegalArgumentException("Error! las coordenadas son iguales");
		else
		{
			this.origen = origen;
			this.destino=destino;
			peso = Herramientas.distanciaEntreCoordenadas(origen, destino);	
		}
	}
	
	@Override
	public boolean equals(Object o)
	{
		boolean resultado = true;
		
		if (o == null && resultado == false )
			return false;
		else
		{
			if (!(o instanceof Arista)) 
				return false;
			else
			{
				Arista arista = (Arista) o;
				if ((arista.destino == origen && arista.origen == destino) || (arista.destino == destino
					&& arista.origen == origen))
					return true;
				else
					return false;
			}
		}
	
	}
	
	public Coordinate getOrigen()
	{
		return origen;
	}
	
	public Coordinate getDestino()
	{
		return destino;
	}
	
	public double getPeso()
	{
		return peso;
	}
}

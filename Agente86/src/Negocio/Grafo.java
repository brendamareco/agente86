package Negocio;

import java.util.ArrayList;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class Grafo 
{
		private ArrayList<Arista> aristas;
		private ArrayList<Coordinate> vertices;
		
		public Grafo()
		{
			aristas = new ArrayList<Arista>();
			vertices = new ArrayList<Coordinate>();
		}
		
		public int cantAristas()
		{
			return aristas.size();
		}
		
		public int cantVertices()
		{
			return vertices.size();
		}

		public void agregarArista(Coordinate origen , Coordinate destino)
		{
			Arista arista = new Arista(origen,destino);
			agregarVertices(arista);
			
			if (!(existeArista(arista)))
			{
				aristas.add(arista);
			}
			
			else throw new IllegalArgumentException("Error! esa arista ya existe");
		}
	
		private void agregarVertices(Arista arista) 
		{
			if (!(vertices.contains(arista.getOrigen())))
			{
				vertices.add(arista.getOrigen());
			}
			
			if (!(vertices.contains(arista.getDestino())))
			{
				vertices.add(arista.getDestino());
			}
			
		}

		public boolean existeArista(Arista arista)
		{
			return aristas.contains(arista);
		}

		public void eliminarArista(Coordinate origen, Coordinate destino)
		{
			Arista arista = new Arista(origen,destino);
		
			if (!(existeArista(arista)))
				throw new IllegalArgumentException("Error, la arista no existe");
			else
				aristas.remove(arista);
		}
		
		public ArrayList<Arista> getAristas()
		{
			return aristas;
		}
		
		public ArrayList<Coordinate> getVertices()
		{
			return vertices;
		}
	
		public double pesoTotal()
		{
			double total = 0;
			for (Arista arista : aristas)
				total = total + arista.getPeso();
			return total;
		}
		
		public ArrayList<Arista> getAristasIncidentes(Coordinate vertice)
		{
			ArrayList<Arista> aristasEnComun = new ArrayList<Arista>();
			for (Arista arista : aristas)
			{
				if (arista.getOrigen().equals(vertice) || arista.getDestino().equals(vertice))
					aristasEnComun.add(arista);
			}
			if (aristasEnComun.isEmpty())
				throw new IllegalArgumentException("Error! no hay ninguna arista con esa coordenada");
			else
				return aristasEnComun;
		}
		
		

		public void convertirGrafoCompleto(ArrayList<Coordinate> ubicacionAgentes) 
		{ 
			//relaciono todos los vertices evitando bucles y que se repitan las aristas
			int posvertice1 = 0;
			int posvertice2 = 1;
			while(posvertice1 < ubicacionAgentes.size() && posvertice2 < ubicacionAgentes.size())
			{
				agregarArista(ubicacionAgentes.get(posvertice1),ubicacionAgentes.get(posvertice2));
				posvertice2++;
				
				if (posvertice2 == ubicacionAgentes.size())
				{
					posvertice1++;
					posvertice2 = posvertice1+1;
				}
			}
			
		}
		
		@Override
		public boolean equals(Object o)
		{
			boolean resultado = true;
			
			if (o == null && resultado == false )
				return false;
			else
			{
				if (!(o instanceof Grafo)) 
					return false;
				else
				{
					Grafo grafo = (Grafo) o;
					resultado = resultado && (grafo.cantAristas()==this.cantAristas());
					for (Arista arista : aristas)
					{
						resultado = resultado && (grafo.getAristas().contains(arista));
					}
				}
			}
			return resultado;
		}

		public Arista getAristaMaxima() 
		{
			if (aristas.size()==0)
				throw new IllegalArgumentException("No hay aristas");
			Arista aristaMax = aristas.get(0);
			double pesoMax = aristas.get(0).getPeso();
			for (Arista arista : aristas)
				if (arista.getPeso() >= pesoMax)
				{
					pesoMax = arista.getPeso();
			        aristaMax = arista;
				}
			return aristaMax;
		}

		public Arista getAristaMinima() 
		{
			if (aristas.size()==0)
				throw new IllegalArgumentException("No hay aristas");
			Arista aristaMin = aristas.get(0);
			double pesoMin = Double.POSITIVE_INFINITY;
			for (Arista arista : aristas)
				if (arista.getPeso() <= pesoMin)
				{
					pesoMin = arista.getPeso();
			        aristaMin = arista;
				}
			return aristaMin;
		}
}

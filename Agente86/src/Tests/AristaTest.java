package Tests;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

import Negocio.Arista;

public class AristaTest 
{
	@Test (expected = IllegalArgumentException.class)
	public void aristaConBucle() 
	{
		Coordinate c1 = new Coordinate (1,22);
		Arista a = new Arista(c1,c1);
	}
	
	@Test
	public void equals ()
	{
		Coordinate c1 = new Coordinate (1,22);
		Coordinate c2 = new Coordinate (12,2);
		Arista a = new Arista(c1,c2);
		Arista b = new Arista(c2,c1);
		assertTrue(a.equals(b));
	}
	
	@Test
	public void notEquals ()
	{
		Coordinate c1 = new Coordinate (1,22);
		Coordinate c2 = new Coordinate (12,2);
		Coordinate c3 = new Coordinate (12,22);
		Arista a = new Arista(c1,c2);
		Arista b = new Arista(c2,c3);
		assertFalse(a.equals(b));
	}
}
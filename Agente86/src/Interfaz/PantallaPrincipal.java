package Interfaz;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

public class PantallaPrincipal
{
	JFrame ventana;
	JPanel contenedor;
	private JPanel panelOpciones, panelTitulo, controles;
	private JLabel titulo;
	private Mapa mapa;
	private JRadioButton mostrarAgentes, mostrarAGM, mostrarCaminoMin, mostrarCaminoMax;
	
	PantallaPrincipal(JFrame ventana)
	{
		this.ventana = ventana;
	}

	public void iniciar() 
	{
		crearContenedor();
		mapa = new Mapa(contenedor);
		crearPaneles();
		mapa.mostrarControlesMapa(controles);
		crearEtiquetas();
		crearRadioButtons();
		crearEventos();
	}
	
	private void crearContenedor() 
	{
		contenedor = new JPanel();
		contenedor.setBounds(0, 0, Configuracion.VENTANA_ANCHO, Configuracion.VENTANA_ALTO);
		contenedor.setLayout(null);
		contenedor.setBackground(Configuracion.COLOR_ARENA);
		ventana.setContentPane(contenedor);
	}
	
	private void crearEventos() 
	{
		//RADIO BUTTON MOSTRAR AGENTES
		mostrarAgentes.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if ((mostrarAgentes.isSelected()))
				{
					mostrarAGM.setSelected(false);
					mostrarCaminoMin.setSelected(false);
					mostrarCaminoMax.setSelected(false);
					mapa.borrarUbicaciones();
				    mapa.mostrarGrafoAgentes();
				}
				else
					mapa.borrarUbicaciones();
			}
		});
		
		//RADIO BUTTON MOSTRAR AGM
		mostrarAGM.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if ((mostrarAGM.isSelected()))
				{
					mostrarAgentes.setSelected(false);
					mostrarCaminoMin.setSelected(false);
					mostrarCaminoMax.setSelected(false);
					mapa.borrarUbicaciones();
				    mapa.mostrarGrafoAGMAgentes();
				}
				else
					mapa.borrarUbicaciones();
			}
		});
		
		//RADIO BUTTON MOSTRAR CAMINO MIN
		mostrarCaminoMin.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if ((mostrarCaminoMin.isSelected()))
				{
					mostrarAgentes.setSelected(false);
					mostrarAGM.setSelected(false);
					mostrarCaminoMax.setSelected(false);
					mapa.borrarUbicaciones();
				    mapa.mostrarCaminoMin();
				}
				else
					mapa.borrarUbicaciones();
			}
		});
				
		//RADIO BUTTON MOSTRAR CAMINO MAX
		mostrarCaminoMax.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if ((mostrarCaminoMax.isSelected()))
				{
					mostrarAgentes.setSelected(false);
					mostrarAGM.setSelected(false);
					mostrarCaminoMin.setSelected(false);
					mapa.borrarUbicaciones();
				    mapa.mostrarCaminoMax();
				}
				else
					mapa.borrarUbicaciones();
			}
		});
	}


	private void crearEtiquetas() 
	{
		titulo = new JLabel("MAPA DE LOS AGENTES");
		titulo.setFont(Configuracion.FUENTE_TITULOS);
		titulo.setHorizontalTextPosition(SwingConstants.CENTER);
		titulo.setVerticalTextPosition(SwingConstants.CENTER);
		titulo.setForeground(Configuracion.COLOR_CREMA);
		titulo.setBounds(Configuracion.PANEL_TITULO_X,
						Configuracion.PANEL_TITULO_Y + Configuracion.MARGEN_CONTENEDOR,
						Configuracion.PANEL_TITULO_ANCHO,
						Configuracion.PANEL_TITULO_ALTO);
		panelTitulo.add(titulo);
	}

	private void crearPaneles() 
	{
		// OPCIONES
		panelOpciones = new JPanel();
		panelOpciones.setBounds(Configuracion.PANEL_OPCIONES_X,
								Configuracion.PANEL_OPCIONES_Y,
								Configuracion.PANEL_OPCIONES_ANCHO,
								Configuracion.PANEL_OPCIONES_ALTO);
		panelOpciones.setBackground(Configuracion.COLOR_MARRON);
		contenedor.add(panelOpciones);
		
		// TITULO
		panelTitulo = new JPanel();
		panelTitulo.setBounds(Configuracion.PANEL_TITULO_X,
				Configuracion.PANEL_TITULO_Y,
				Configuracion.PANEL_TITULO_ANCHO,
				Configuracion.PANEL_TITULO_ALTO);
		panelTitulo.setBackground(Configuracion.COLOR_MARRON);
		contenedor.add(panelTitulo);
		
		// CONTROLES
		controles = new JPanel();
		controles.setBounds(Configuracion.BOTON_DIRECCIONES_X,
				Configuracion.BOTON_DIRECCIONES_Y,
				Configuracion.PANEL_CONTROLES_ANCHO,
				Configuracion.PANEL_CONTROLES_ALTO);
		controles.setBackground(Configuracion.COLOR_MARRON);
		panelOpciones.add(controles);
	}	
	
	private void crearRadioButtons() 
	{
		//MOSTRAR AGENTES
		mostrarAgentes = new JRadioButton("Mostrar agentes");
		mostrarAgentes.setBackground(null);
		mostrarAgentes.setFont(Configuracion.FUENTE_TEXTOS);
		mostrarAgentes.setBounds(Configuracion.RADIOBUTTON_MOSTRAR_AGENTES_X,
				Configuracion.RADIOBUTTON_MOSTRAR_AGENTES_Y,
				Configuracion.RADIOBUTTONS_ANCHO,
				Configuracion.RADIOBUTTONS_ALTO);
		panelOpciones.add(mostrarAgentes);
		
		//MOSTRAR AGM
		mostrarAGM = new JRadioButton("Mostrar AGM");
		mostrarAGM.setBackground(null);
		mostrarAGM.setFont(Configuracion.FUENTE_TEXTOS);
		mostrarAGM.setBounds(Configuracion.RADIOBUTTON_MOSTRAR_AGM_X,
				Configuracion.RADIOBUTTON_MOSTRAR_AGM_Y,
				Configuracion.RADIOBUTTONS_ANCHO,
				Configuracion.RADIOBUTTONS_ALTO);
		panelOpciones.add(mostrarAGM);
		
		//MOSTRAR CAMINO MINIMO
		mostrarCaminoMin = new JRadioButton("Mostrar camino más corto entre dos agentes");
		mostrarCaminoMin.setBackground(null);
		mostrarCaminoMin.setFont(Configuracion.FUENTE_TEXTOS);
		mostrarCaminoMin.setBounds(Configuracion.RADIOBUTTON_MOSTRAR_AGM_X,
				Configuracion.RADIOBUTTON_MOSTRAR_AGM_Y,
				Configuracion.RADIOBUTTONS_ANCHO,
				Configuracion.RADIOBUTTONS_ALTO);
		panelOpciones.add(mostrarCaminoMin);
		
		//MOSTRAR CAMINO MAXIMO
		mostrarCaminoMax = new JRadioButton("Mostrar camino más largo entre dos agentes");
		mostrarCaminoMax.setBackground(null);
		mostrarCaminoMax.setFont(Configuracion.FUENTE_TEXTOS);
		mostrarCaminoMax.setBounds(Configuracion.RADIOBUTTON_MOSTRAR_AGM_X,
				Configuracion.RADIOBUTTON_MOSTRAR_AGM_Y,
				Configuracion.RADIOBUTTONS_ANCHO,
				Configuracion.RADIOBUTTONS_ALTO);
		panelOpciones.add(mostrarCaminoMax);
	}
}

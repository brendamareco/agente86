package Interfaz;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import Negocio.Arista;
import Negocio.Grafo;
import Negocio.Prim;

public class Mapa 
{	
	private JMapViewer mapa;
	private JButton botonIzq, botonDer, botonArriba, botonAbajo;
	public Grafo agentes;
	public Grafo agentesAGM;
	
	public Mapa(JPanel contenedor)
	{
		crearAgentes();
		crearMapa(contenedor);
		crearBotones();
	}
	
	private void crearAgentes() 
	{
		agentes = new Grafo();
	
		ArrayList<Coordinate>ubicacionAgentes = new ArrayList<Coordinate>();
		ubicacionAgentes.add(Configuracion.ARGENTINA);
		ubicacionAgentes.add(Configuracion.JAPON); 
		ubicacionAgentes.add(Configuracion.ALEMANIA); 
		ubicacionAgentes.add(Configuracion.RUSIA); 
		ubicacionAgentes.add(Configuracion.AUSTRALIA); 
		ubicacionAgentes.add(Configuracion.SPAIN);
		ubicacionAgentes.add(Configuracion.BRASIL);
		ubicacionAgentes.add(Configuracion.SUDAFRICA);
		ubicacionAgentes.add(Configuracion.TURQUIA);
		ubicacionAgentes.add(Configuracion.USA);
	
		agentes.convertirGrafoCompleto(ubicacionAgentes);
	    agentesAGM = Prim.getAGM(agentes);
	}

	public void mostrarGrafoAgentes()
	{
		for (Arista camino : agentes.getAristas())
		{
			mostrarMapaPath(camino.getOrigen(), camino.getDestino());
		}
	}
	
	public void mostrarGrafoAGMAgentes()
	{
		for (Arista camino : agentesAGM.getAristas())
		{
			mostrarMapaPath(camino.getOrigen(), camino.getDestino());
		}
	}
	
	public void mostrarCaminoMax() 
	{
		Arista aristaMax = agentesAGM.getAristaMaxima();
		mostrarMapaPath(aristaMax.getOrigen(), aristaMax.getDestino());
	}

	public void mostrarCaminoMin() 
	{
		Arista aristaMinima = agentesAGM.getAristaMinima();
		mostrarMapaPath(aristaMinima.getOrigen(), aristaMinima.getDestino());
	}
	
	private void crearBotones() 
	{
		//BOTON IZQUIERDO
		botonIzq = new JButton();
		botonIzq.setBounds(Configuracion.BOTON_DIRECCIONES_X,
				Configuracion.BOTON_DIRECCIONES_Y,
				Configuracion.BOTON_DIRECCIONES_TAMANO,
				Configuracion.BOTON_DIRECCIONES_TAMANO);
		botonIzq.setIcon(new ImageIcon(Herramientas.cargarImagenConEscala("left.png",
				         botonIzq.getWidth(), botonIzq.getHeight())));
		agregarEventoBotonControl(botonIzq,10,0);
		
		//BOTON DERECHO
		botonDer = new JButton();
		botonDer.setBounds(botonIzq.getX() + botonIzq.getWidth() + 5,
				Configuracion.BOTON_DIRECCIONES_Y,
				Configuracion.BOTON_DIRECCIONES_TAMANO,
				Configuracion.BOTON_DIRECCIONES_TAMANO);
		botonDer.setIcon(new ImageIcon(Herramientas.cargarImagenConEscala("right.png",
				botonDer.getWidth(), botonDer.getHeight())));
		agregarEventoBotonControl(botonDer,-10,0);
		
		//BOTON ARRIBA
		botonArriba = new JButton();
		botonArriba.setBounds(botonDer.getX() + botonDer.getWidth() + 5,
				Configuracion.BOTON_DIRECCIONES_Y,
				Configuracion.BOTON_DIRECCIONES_TAMANO,
				Configuracion.BOTON_DIRECCIONES_TAMANO);
		botonArriba.setIcon(new ImageIcon(Herramientas.cargarImagenConEscala("up.png",
				botonArriba.getWidth(), botonArriba.getHeight())));
		agregarEventoBotonControl(botonArriba,0,10);
		
		//BOTON ABAJO
		botonAbajo = new JButton();
		botonAbajo.setBounds(botonArriba.getX() + botonArriba.getWidth() + 5,
				Configuracion.BOTON_DIRECCIONES_Y,
				Configuracion.BOTON_DIRECCIONES_TAMANO,
				Configuracion.BOTON_DIRECCIONES_TAMANO);
		botonAbajo.setIcon(new ImageIcon(Herramientas.cargarImagenConEscala("down.png",
				botonAbajo.getWidth(), botonAbajo.getHeight())));
		agregarEventoBotonControl(botonAbajo,0,-10);
	}

	private void agregarEventoBotonControl(JButton boton, final int x, final int y) {
		boton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				moverMapa(x,y);
			}
		});
	}

	private void crearMapa(JPanel contenedor) 
	{
		mapa = new JMapViewer();
		mapa.setBounds(Configuracion.MAPA_X, Configuracion.MAPA_Y,
				Configuracion.MAPA_ANCHO, Configuracion.MAPA_ALTO);
		mapa.setBackground(Configuracion.COLOR_CREMA);
		mapa.setZoom(2);
		mapa.moveMap(0, Configuracion.VENTANA_ALTO/9);
		contenedor.add(mapa);
	}

	private void moverMapa(int x,int y)
	{
		mapa.moveMap(x, y);
	}

	public void mostrarControlesMapa(JPanel contenedor)
	{
		contenedor.add(botonIzq);
		contenedor.add(botonDer);
		contenedor.add(botonArriba);
		contenedor.add(botonAbajo);
	}
	
	private void mostrarAgente(Coordinate agente)
	{
		MapMarkerDot posicion = new MapMarkerDot(agente);
		posicion.setBackColor(Configuracion.COLOR_ARENA);
		posicion.setFont(Configuracion.FUENTE_TEXTOS);
		if (agente.equals(Configuracion.ARGENTINA))
			posicion.setName("ARGENTINA");
		if (agente.equals(Configuracion.ALEMANIA))
			posicion.setName("ALEMANIA");
		if (agente.equals(Configuracion.SPAIN))
			posicion.setName("ESPA�A");
		if (agente.equals(Configuracion.RUSIA))
			posicion.setName("RUSIA");
		if (agente.equals(Configuracion.SUDAFRICA))
			posicion.setName("SUDAFRICA");
		if (agente.equals(Configuracion.USA))
			posicion.setName("EEUU");
		if (agente.equals(Configuracion.BRASIL))
			posicion.setName("BRASIL");
		if (agente.equals(Configuracion.AUSTRALIA))
			posicion.setName("AUSTRALIA");
		if (agente.equals(Configuracion.TURQUIA))
			posicion.setName("TURQU�A");
		if (agente.equals(Configuracion.JAPON))
			posicion.setName("JAP�N");
		    
		mapa.addMapMarker(posicion);
	}
	
	private void mostrarMapaPath(Coordinate c1, Coordinate c2)
	{
		ArrayList<Coordinate> path = new ArrayList<Coordinate>();
		path.add(c1);
		path.add(c2);
		path.add(c2);
		
		MapPolygonImpl linea = new MapPolygonImpl(path);
		linea.setColor(Configuracion.COLOR_MARRON);
		double distancia = Herramientas.distanciaEntreCoordenadas(c1, c2);
		linea.setName(distancia+" km");
		linea.setFont(Configuracion.FUENTE_MAPA);
		mostrarAgente(c1);
		mostrarAgente(c2);
		mapa.addMapPolygon(linea);
	}
	
	public void borrarUbicaciones()
	{
		mapa.removeAllMapMarkers();
		mapa.removeAllMapPolygons();
	}
}

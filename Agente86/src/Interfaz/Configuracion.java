package Interfaz;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class Configuracion 
{
	public static final Dimension RESOLUCION_PANTALLA = Toolkit.getDefaultToolkit().getScreenSize();
	
	public static final int VENTANA_ALTO = (int) RESOLUCION_PANTALLA.getHeight();
	public static final int VENTANA_ANCHO = (int) RESOLUCION_PANTALLA.getWidth();
	public static final int VENTANA_TITULO = 39;
	
	public static final int MARGEN_CONTENEDOR = 5;
	
	public static final int PANEL_TITULO_Y = 0;
	public static final int PANEL_TITULO_X = 0;
	public static final int PANEL_TITULO_ANCHO = VENTANA_ANCHO;
	public static final int PANEL_TITULO_ALTO = 40;
	
	public static final int MAPA_X = PANEL_TITULO_X;
	public static final int MAPA_Y = PANEL_TITULO_Y + PANEL_TITULO_ALTO + MARGEN_CONTENEDOR;
	public static final int MAPA_ANCHO = PANEL_TITULO_ANCHO;
	public static final int MAPA_ALTO = MAPA_Y + VENTANA_ANCHO/2 - 125;
		
	public static final int PANEL_OPCIONES_Y = MAPA_Y + MAPA_ALTO + MARGEN_CONTENEDOR;
	public static final int PANEL_OPCIONES_X = MAPA_X;
	public static final int PANEL_OPCIONES_ANCHO = MAPA_ANCHO;
	public static final int PANEL_OPCIONES_ALTO = VENTANA_ALTO - MARGEN_CONTENEDOR*2 - VENTANA_TITULO;
	
	public static final int BOTON_INICIAR_ANCHO = 40;
	public static final int BOTON_INICIAR_ALTO = 20;
	public static final int BOTON_INICIAR_X = VENTANA_ANCHO/2 - BOTON_INICIAR_ANCHO/2;
	public static final int BOTON_INICIAR_Y = (VENTANA_ALTO + VENTANA_TITULO)/2 - BOTON_INICIAR_ALTO/2;
	
	public static final int BOTON_DIRECCIONES_TAMANO = 10;
	public static final int BOTON_DIRECCIONES_X = MAPA_X + MAPA_ANCHO/2;
	public static final int BOTON_DIRECCIONES_Y = MAPA_Y + MAPA_ALTO + MARGEN_CONTENEDOR*2;
	
	public static final int PANEL_CONTROLES_ANCHO = BOTON_DIRECCIONES_TAMANO*4 + 5*5;
	public static final int PANEL_CONTROLES_ALTO = BOTON_DIRECCIONES_TAMANO;
	
	public static final int RADIOBUTTONS_ANCHO = 100;
	public static final int RADIOBUTTONS_ALTO = 20;
	
	public static final int RADIOBUTTON_MOSTRAR_AGM_X = MARGEN_CONTENEDOR;
	public static final int RADIOBUTTON_MOSTRAR_AGM_Y = BOTON_DIRECCIONES_Y + MARGEN_CONTENEDOR;
	
	public static final int RADIOBUTTON_MOSTRAR_AGENTES_X = RADIOBUTTON_MOSTRAR_AGM_X;
	public static final int RADIOBUTTON_MOSTRAR_AGENTES_Y = RADIOBUTTON_MOSTRAR_AGM_Y + RADIOBUTTONS_ALTO + MARGEN_CONTENEDOR;
	
	public static Font FUENTE_TEXTOS_BORDE = new Font("Yu Gothic UI Light", Font.PLAIN, 14);
	public static final Font FUENTE_TEXTOS = new Font("Yu Gothic UI Light", Font.BOLD, 14);
	public static final Font FUENTE_TITULOS = new Font("Yu Gothic UI SemiLight", Font.BOLD, 18);
	public static final Font FUENTE_MAPA = new Font("Yu Gothic UI SemiLight", Font.PLAIN,12);

	public static final Color COLOR_ARENA = new Color(227,203,179);
	public static final Color COLOR_SALMON = new Color(241,181,181);
	public static final Color COLOR_MARRON = new Color(183,148,129);
	public static final Color COLOR_CREMA = new Color(255,243,230);
	
	public static final Coordinate ARGENTINA = new Coordinate(-34.61315,-58.37723);
	public static final Coordinate JAPON = new Coordinate(36,138);
	public static final Coordinate ALEMANIA = new Coordinate(51,9);
	public static final Coordinate RUSIA = new Coordinate(60,100);
	public static final Coordinate AUSTRALIA = new Coordinate(-27,133);
	public static final Coordinate SPAIN = new Coordinate(40,-4);
	public static final Coordinate BRASIL = new Coordinate(-10,-55);
	public static final Coordinate SUDAFRICA = new Coordinate(-29,24);
	public static final Coordinate TURQUIA = new Coordinate(39,35);
	public static final Coordinate USA = new Coordinate(38,-97);
}


package Negocio;

import java.util.ArrayList;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class Prim 
{
	public static Grafo getAGM(Grafo g)
	{
		if (g.cantAristas()==0)
			throw new IllegalArgumentException ("Error! grafo vacio");
		
		ArrayList<Coordinate> verticesG = g.getVertices();
		ArrayList<Coordinate> verticesRecorridos = new ArrayList<Coordinate>(); 
		Grafo AGM = new Grafo();
		verticesRecorridos.add(verticesG.get(0));
		double pesoMinimo = Double.POSITIVE_INFINITY;
		Arista aristaMinima = null;
		int i = 1;
		
		while (i < verticesG.size())
		{
			for (Coordinate vertice : verticesRecorridos)
			{
				Arista aristaElegida = getAristaMinima(vertice,verticesRecorridos,g);
				if (aristaElegida.getPeso() <= pesoMinimo)
				{
					pesoMinimo = aristaElegida.getPeso();
					aristaMinima = aristaElegida;
				}
			}
			pesoMinimo = Double.POSITIVE_INFINITY;
			
			if (!(verticesRecorridos.contains(aristaMinima.getOrigen())))
				verticesRecorridos.add(aristaMinima.getOrigen());
				
		    else if (!(verticesRecorridos.contains(aristaMinima.getDestino())))
		    	verticesRecorridos.add(aristaMinima.getDestino());
			
			i++;
			AGM.agregarArista(aristaMinima.getOrigen(), aristaMinima.getDestino());
		
		}
		return AGM;
	}
	
	public static Arista getAristaMinima(Coordinate vertice, ArrayList<Coordinate> verticesRecorridos, Grafo g)
	{
		ArrayList<Arista> incidentesAVertice = g.getAristasIncidentes(vertice);
		double pesoMinimo = Double.POSITIVE_INFINITY;
		Arista aristaMinima = null;
		for (Arista arista : incidentesAVertice)
		{
			if (!(verticesRecorridos.contains(arista.getOrigen())) || !(verticesRecorridos.contains(arista.getDestino())))
			{
		      if (arista.getPeso() <= pesoMinimo)
		      {
		    	  pesoMinimo = arista.getPeso();
		    	  aristaMinima = arista;
		      }
			}
		}
		return aristaMinima;
	}
}
